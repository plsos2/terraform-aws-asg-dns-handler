## [2.1.7](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.1.6...v2.1.7) (2021-03-27)


### Bug Fixes

* append list ([dfebd17](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/dfebd17fced050f65d74c436b660abe3158e22c3))

## [2.1.6](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.1.5...v2.1.6) (2021-03-27)


### Bug Fixes

* delete single record first ([3cc5395](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/3cc5395c734c84d7628b217fba88623753fca50a))
* filter terminated ip ([6bd52c5](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/6bd52c5ce87b61bccd3f716befef9905fcd244cb))

## [2.1.5](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.1.4...v2.1.5) (2021-03-27)


### Bug Fixes

* fetch route ip ([996be05](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/996be05f31b97ea115e0e53dc3176e6da88b3857))

## [2.1.4](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.1.3...v2.1.4) (2021-03-27)


### Bug Fixes

* create record for each instance ([0fda546](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/0fda546badaa6138e1ce1c77d4d0e62f020a60fe))

## [2.1.3](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.1.2...v2.1.3) (2021-03-27)


### Bug Fixes

* condional ([a66e8c8](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/a66e8c8576bd278c26996a7575f0fb368f1d62c7))

## [2.1.2](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.1.1...v2.1.2) (2021-03-27)


### Bug Fixes

* resource records ([b77ea3f](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/b77ea3f694d5c2e5c5650a3ad987641c1c1c16a9))

## [2.1.1](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.1.0...v2.1.1) (2021-03-27)


### Bug Fixes

* always get instance id ([e4a2e3e](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/e4a2e3ea52ecba38b5e8c01791f8c9d17c45e819))

# [2.1.0](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.15...v2.1.0) (2021-03-27)


### Features

* adding remote A records on termination ([0bc178e](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/0bc178eb7ddb8f20c4bca8c401aaafd98c36dd41))

## [2.0.15](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.14...v2.0.15) (2021-03-26)


### Bug Fixes

* ResourceRecordSets key name ([323c373](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/323c373e7eb1ec8533c1ebcdc0fa00e31b735f56))

## [2.0.14](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.13...v2.0.14) (2021-03-26)


### Bug Fixes

* debug results ([08e3697](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/08e3697292ae555fa5a389f8fc66c9319929e1bf))

## [2.0.13](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.12...v2.0.13) (2021-03-26)


### Bug Fixes

* exising records ([4f8e24d](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/4f8e24da2a3c763c1659960e37c8d2b76c912bec))

## [2.0.12](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.11...v2.0.12) (2021-03-26)


### Bug Fixes

* add debug results ([9de4aea](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/9de4aeae96a10539ab38e46bc08a8f5194ece9a3))

## [2.0.11](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.10...v2.0.11) (2021-03-26)


### Bug Fixes

* exists check ([2e58c3b](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/2e58c3b00e081a975d413f0bbcd5747f15a0d74f))

## [2.0.10](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.9...v2.0.10) (2021-03-26)


### Bug Fixes

* typo ([3d755b2](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/3d755b29dca104b54cf0086d56f6935c14407041))

## [2.0.9](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.8...v2.0.9) (2021-03-26)


### Bug Fixes

* exist check ([846cb3a](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/846cb3a24fcb9c0b911ffac933b552edf311a239))

## [2.0.8](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.7...v2.0.8) (2021-03-26)


### Bug Fixes

* record check ([f4e5ff7](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/f4e5ff791f0a51fab2e23ffa7153837953a34e9f))

## [2.0.7](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.6...v2.0.7) (2021-03-26)


### Bug Fixes

* exists check ([c7d02ef](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/c7d02ef3c03174f350c55a92ce1247e6b67ec7af))

## [2.0.6](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.5...v2.0.6) (2021-03-26)


### Bug Fixes

* existing records ([3170151](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/317015172d924766f94bdf96fba9e74b25d9c5f2))

## [2.0.5](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.4...v2.0.5) (2021-03-26)


### Bug Fixes

* need new record check ([a0cf511](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/a0cf5114ea65337536b8ff4b836121a309442114))

## [2.0.4](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.3...v2.0.4) (2021-03-26)


### Bug Fixes

* records may not exist ([d23e1df](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/d23e1df9d4376b6415b5e1bb12c68f37866169ae))

## [2.0.3](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.2...v2.0.3) (2021-03-26)


### Bug Fixes

* merge records ([29851ad](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/29851adf8c7975f4a44e34164ecf9341f3fcf0a5))

## [2.0.2](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.1...v2.0.2) (2021-03-26)


### Bug Fixes

* cluster name record ([06a4acb](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/06a4acb8daaae6ffdb2a5d24a79292c83b489859))

## [2.0.1](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/compare/v2.0.0...v2.0.1) (2020-10-18)


### Bug Fixes

* python script error when no lifecycle ([cc996b0](https://bitbucket.org/plsos2/terraform-aws-asg-dns-handler/commits/cc996b0f35d0b36e441886dc0370cbc526d5e8c2))
